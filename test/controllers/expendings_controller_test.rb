require 'test_helper'

class ExpendingsControllerTest < ActionController::TestCase
  setup do
    @expending = expendings(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:expendings)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create expending" do
    assert_difference('Expending.count') do
      post :create, expending: { account_id: @expending.account_id, fecha: @expending.fecha, monto: @expending.monto }
    end

    assert_redirected_to expending_path(assigns(:expending))
  end

  test "should show expending" do
    get :show, id: @expending
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @expending
    assert_response :success
  end

  test "should update expending" do
    patch :update, id: @expending, expending: { account_id: @expending.account_id, fecha: @expending.fecha, monto: @expending.monto }
    assert_redirected_to expending_path(assigns(:expending))
  end

  test "should destroy expending" do
    assert_difference('Expending.count', -1) do
      delete :destroy, id: @expending
    end

    assert_redirected_to expendings_path
  end
end
