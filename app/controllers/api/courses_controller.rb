class Api::CoursesController < ApplicationController

  def registrar_pago
    @course = Course.find(params[:id])
    if payment = @course.registrar_pago(params[:periodo_pago], params[:monto], params[:fecha])
      render json: payment, status: :ok
    else
      render json: nil, status: :unprocessable_entity
    end
  end

  def baja
    @course = Course.find(params[:id])
    if @course.update(baja: true, fecha_baja: Date.today)
      render json: @course, status: :ok
    else
      render json: nil, status: :unprocessable_entity
    end
  end
end
