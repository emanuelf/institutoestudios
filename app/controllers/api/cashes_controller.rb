class Api::CashesController < ApplicationController

  def set_monto_inicial
    @cash = Cash.create(monto_inicial: params[:monto_inicial], fecha: Date.today)
    render json: {message: 'Listo'}, status: :ok
  end

  def cerrar
    @cash = Cash.find(params[:id])
    if @cash.update(cerrada: true)
      render json: {message: 'Caja cerrada'}, status: :ok
    else
      render json: {message: 'Problemas'}, status: :unprocessable_entity
    end
  end
end
