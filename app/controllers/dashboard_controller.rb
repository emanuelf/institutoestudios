class DashboardController < ApplicationController
  def index
    redirect_to courses_path
  end

  def estados_de_cuenta
    @gastos = {}
    Study.all.each do |study|
      @gastos[study.id] = study.gastos_entre(params[:inicio], params[:fin])
    end
    inicio  = params[:inicio].present? ? params[:inicio] : (Date.today - 1000.years).to_s
    fin     = params[:fin].present? ? params[:fin] : (Date.today + 1000.years).to_s
 
    @otros_gastos_periodo   = Expending.joins(:account).where
                                       .not(account_id: StudyExpendingAccount.pluck(:account_id))
                                       .where(fecha: inicio..fin)
                                       .group('accounts.nombre').sum('monto')
    @otros_ingresos_periodo = Entry.joins(:account)
                                   .where(fecha: inicio..fin)
                                   .group('accounts.nombre').sum('monto')
  end
end
