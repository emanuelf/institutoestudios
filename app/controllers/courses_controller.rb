class CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.search(params[:periodo_adeudado], params[:studies_ids])
    if params[:con_requisitos_faltantes] == '1'
      @courses = @courses.select {|c| c.requisitos_no_cumplidos? }
    end
    if params[:anio_en_curso].present?
      @courses = @courses.select { |c| 
        c.anio_en_curso_number == params[:anio_en_curso].to_i 
      }
    end
  end

  def planilla_notas
    @courses = Course.where(id: params[:courses_ids].split(','))
    @titulo = 'Planilla notas'
    render 'pdfs/planilla', layout: false
  end

  def planilla_asistencia
    @courses = Course.where(id: params[:courses_ids].split(','))
    @titulo = 'Planilla asistencia'
    render 'pdfs/planilla', layout: false
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
  end

  # GET /courses/new
  def new
    @course = Course.new
    @course.build_student
  end

  # GET /courses/1/edit
  def edit
    @student = @course.student || Student.new
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.new(course_params)

    respond_to do |format|
      if @course.save
        format.html { redirect_to courses_path, notice: 'Course was successfully created.' }
        format.json { render :show, status: :created, location: @course }
      else
        format.html { render :new }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    #byebug
    cparams = course_params
    if cparams[:requisitos].present? == false
      cparams[:requisitos] = []
    end
    respond_to do |format|
      if @course.update(cparams) 
        format.html { redirect_to courses_path, notice: 'Course was successfully updated.' }
        format.json { render :show, status: :ok, location: @course }
      else
        format.html { render :edit }
        format.json { render json: @course.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    @course.destroy
    respond_to do |format|
      format.html { redirect_to courses_url, notice: 'Course was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:beca, :study_id, :anio_inicio, student_attributes: [:nombre_completo, :dni, :comentarios, :direccion, requisitos: []])
    end
end
