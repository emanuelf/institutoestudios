class ExpendingsController < ApplicationController
  before_action :set_expending, only: [:show, :edit, :update, :destroy]

  def index
    @expendings = Expending.search(params)
  end

  def show
  end

  def new
    @expending = Expending.new
  end

  def edit
  end

  def create
    @expending = Expending.new(expending_params)

    respond_to do |format|
      if @expending.save
        format.html { redirect_to expendings_path, notice: 'Expending was successfully created.' }
      else
        format.html { render :new }
        format.json { render json: @expending.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @expending.update(expending_params)
        format.html { redirect_to @expending, notice: 'Expending was successfully updated.' }
        format.json { render :show, status: :ok, location: @expending }
      else
        format.html { render :edit }
        format.json { render json: @expending.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @expending.destroy
    respond_to do |format|
      format.html { redirect_to expendings_url, notice: 'Expending was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    def set_expending
      @expending = Expending.find(params[:id])
    end

    def expending_params
      params.require(:expending).permit(:fecha, :monto, :account_id)
    end
end
