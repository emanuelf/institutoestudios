class CashesController < ApplicationController

  def today
    @cash = Cash.find_or_initialize_by(fecha: Date.today)
  end

  def index
    @cashes = Cash.order(fecha: :desc)
  end

  def show
    @cash = Cash.find(params[:id])
    render :today
  end
end
