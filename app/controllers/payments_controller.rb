include AfipBillHelper

class PaymentsController < ApplicationController
  def index
  end

  def gen_comprobante
    @payment = Payment.find(params[:id])
    if @payment.course.study.carrera?
      neto = @payment.monto
      factura = Bravo::Bill.new(iva_condition: :consumidor_final, net: neto.round(2).to_f, invoice_type: :invoice)
      factura.document_number      = @payment.course.student.dni 
      factura.document_type        = 'DNI'
      if factura.authorize
        response = factura.response
        @payment.update(cae: response[:cae], nro_factura: response[:cbte_hasta])
        send_data gen_afip_pdf(response.to_json, @payment), filename: 'factura.pdf'
      else
        send_data(render_to_string('ERROR AL GENERAR LA FACTURA ELECTRÓNICA', layout: false))
      end
    else
      @payment.update(nro_recibo: Payment.maximum(:nro_recibo).to_i + 1)
      pdf = WickedPdf.new.pdf_from_string(
        render_to_string('pdfs/recibo.html.erb', layout: false),  orientation: 'Landscape'
      )
      send_data pdf, filename: 'recibo.pdf'
    end
  end
end
