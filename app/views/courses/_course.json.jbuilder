json.extract! course, :id, :study_id, :student_id, :ciclo, :created_at, :updated_at
json.url course_url(course, format: :json)
