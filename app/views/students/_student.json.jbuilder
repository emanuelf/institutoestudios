json.extract! student, :id, :nombre_completo, :dni, :matricula, :direccion, :comentarios, :created_at, :updated_at
json.url student_url(student, format: :json)
