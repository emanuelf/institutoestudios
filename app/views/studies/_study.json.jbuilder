json.extract! study, :id, :nombre, :descripcion, :tipo_estudio, :duracion, :created_at, :updated_at
json.url study_url(study, format: :json)
