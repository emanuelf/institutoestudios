json.extract! entry, :id, :fecha, :monto, :account_id, :created_at, :updated_at
json.url entry_url(entry, format: :json)
