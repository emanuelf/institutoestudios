$(function() {
  
  $('button.registrar-pago').click(function(event) {
    var modal = $('#modal-pago');
    modal.find('#pago-course-id').val($(this).data('id'));
    modal.find('#pago-nombre-completo').val($(this).data('nombre-completo'));
    modal.find('#pago-nombre-estudio').val($(this).data('nombre-estudio'));
    modal.find('#pago-periodo').val($(this).data('periodo')).trigger('change');
    modal.find('#pago-monto-cuota').val($(this).data('monto-cuota'));
    modal.find('#pago-beca').val($(this).data('beca'));
    var montoAPagar = parseFloat($(this).data('monto-a-pagar'));
    if ( parseFloat($(this).data('interes')) != 0 ) {
      modal.find('#pago-interes').html("+ $" + $(this).data('interes') + " de interés");
      modal.find('#pago-interes').removeClass('hidden')
      montoAPagar += parseFloat($(this).data('interes'));
    } else {
      modal.find('#pago-interes').addClass('hidden')
    }
    modal.find('#pago-monto-a-pagar').val(montoAPagar);
    modal.modal();
  });

  $('#enviar-pago').click(function(event) {
    var monto = $('#modal-pago').find('#pago-monto-cuota').val();
    var params = {
      id: $('#modal-pago #pago-course-id').val(), 
      monto: monto, 
      periodo_pago: $('#modal-pago #pago-periodo').val(), 
      fecha: $('#pago-fecha').val()
    }
    $.post('/api/courses/registrarpago', params)
      .done(function(data) {
        $('#modal-pago').modal('hide');
        window.location = '/payments/' + data.id + '/recibo'
        alert('¡Pago registrado!');
      })
      .fail(function(error) {
        alert('¡Errores al registrar el pago!')
      });
  });
});