# == Schema Information
#
# Table name: entries
#
#  id         :integer          not null, primary key
#  fecha      :date
#  monto      :decimal(, )
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class EntrySerializer < ActiveModel::Serializer
  attributes :id, :fecha, :monto
  has_one :account
end
