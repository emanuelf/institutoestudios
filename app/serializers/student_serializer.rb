# == Schema Information
#
# Table name: students
#
#  id              :integer          not null, primary key
#  nombre_completo :string
#  dni             :string(15)
#  matricula       :integer
#  direccion       :string
#  comentarios     :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class StudentSerializer < ActiveModel::Serializer
  attributes :id, :nombre_completo, :dni, :matricula, :direccion, :comentarios
end
