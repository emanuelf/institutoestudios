# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  nombre     :string
#  tipo       :string(15)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class AccountSerializer < ActiveModel::Serializer
  attributes :id, :nombre, :tipo
end
