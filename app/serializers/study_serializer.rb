# == Schema Information
#
# Table name: studies
#
#  id           :integer          not null, primary key
#  nombre       :string(100)
#  descripcion  :string
#  tipo_estudio :string(20)
#  duracion     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  monto_cuota  :decimal(, )      not null
#  requisitos   :text
#

class StudySerializer < ActiveModel::Serializer
  attributes :id, :nombre, :descripcion, :tipo_estudio, :duracion
end
