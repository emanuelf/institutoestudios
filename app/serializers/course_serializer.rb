# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  study_id    :integer
#  student_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  anio_inicio :integer
#  beca        :decimal(, )      default(0.0)
#  requisitos  :text
#  baja        :boolean
#  fecha_baja  :date
#

class CourseSerializer < ActiveModel::Serializer
  attributes :id, :anio_inicio
  has_one :study
  has_one :student
end
