# == Schema Information
#
# Table name: expendings
#
#  id         :integer          not null, primary key
#  fecha      :date
#  monto      :decimal(, )
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class ExpendingSerializer < ActiveModel::Serializer
  attributes :id, :fecha, :monto
  has_one :account
end
