module AfipBillHelper
      
  def gen_afip_pdf(bravo_response, payment)
    user = AfipBill::User.new("Fundación Carola B.",
                          payment.course.student.nombre_completo,
                          payment.course.student.direccion,
                          "Consumidor final")
    item_1 = AfipBill::LineItem.new("Cuota periodo " + payment.periodo_pago, 1, payment.monto)

    generator = AfipBill::Generator.new(bravo_response, user, [item_1])
    generator.generate_pdf_string
  end
end
