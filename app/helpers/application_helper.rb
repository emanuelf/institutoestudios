module ApplicationHelper
  def number_class(number)
    if number > 0
      'label-success'
    elsif number < 0
      'label-danger'
    else
      'label-default'
    end
  end
end
