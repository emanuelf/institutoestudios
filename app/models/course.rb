# == Schema Information
#
# Table name: courses
#
#  id          :integer          not null, primary key
#  study_id    :integer
#  student_id  :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  anio_inicio :integer
#  beca        :decimal(, )      default(0.0)
#  requisitos  :text
#  baja        :boolean
#  fecha_baja  :date
#

class Course < ActiveRecord::Base
  
  belongs_to :study
  belongs_to :student
  has_many :payments

  serialize :requisitos
  validates_presence_of :study_id

  accepts_nested_attributes_for :student
  
  after_validation :asignar_monto_cuota

  before_create :verify_null_requisitos

  def self.search(periodo_adeudado, ids_studies = [])
    if !periodo_adeudado.present? && !ids_studies.present?
      return self.all
    end
    ids_studies = [] if ids_studies.present? == false
    ids_studies.delete("")
    if ids_studies.empty?
      ids_studies = Study.pluck(:id)
    end
    if periodo_adeudado.present?
      Course.where
            .not(id: Payment.where('periodo_pago': periodo_adeudado).pluck(:course_id))
            .where(study_id: ids_studies)

    else 
      Course.where(study_id: ids_studies)
    end
  end

  def verify_null_requisitos
    self.requisitos = [] if self.requisitos.nil?
  end

  def asignar_monto_cuota
    #self.monto_cuota = study.monto_cuota
  end

  def ultima_cuota_pagada
    payments.last
  end

  def siguiente_periodo_a_pagar
    ult_cuota_pag = ultima_cuota_pagada
    if ult_cuota_pag 
      periodo = ultima_cuota_pagada.periodo
      if !ult_cuota_pag.saldo?
        periodo[:mes] = periodo[:mes].to_i + 1
      end
      "#{periodo[:anio]}/#{periodo[:mes].to_s.rjust(2,'0')}"
    else 
      periodo = "#{Date.today.year}/#{(Date.today.month - 1 ).to_s.rjust(2,'0')}"
    end
  end

  def monto_adeudado
    anterior_pago_periodo = payments.where(periodo_pago: siguiente_periodo_a_pagar).last
    if anterior_pago_periodo.nil? 
      return study.monto_cuota
    else 
      if anterior_pago_periodo.saldo?
        return anterior_pago_periodo.saldo
      else
        return study.monto_cuota
      end
    end
  end

  def ultimo_periodo_pago
    ultima_cuota_pagada.periodo_pago
  end

  def monto_a_pagar
    study.monto_cuota - beca
  end

  def requisitos_cumplidos?
    (study.requisitos - self.requisitos ).empty?
  end

  def requisitos_no_cumplidos
    study.requisitos - self.requisitos 
  end

  def requisitos_no_cumplidos?
    !requisitos_cumplidos?
  end

  def requisito_cumplido? req
    if requisitos
      requisitos.include? req 
    else
      false
    end
  end

  def anio_en_curso
    return "Primero" if anio_inicio == Date.today.year
    return "Segundo" if anio_inicio == Date.today.year - 1
    return "Tercero" if anio_inicio == Date.today.year - 2
  end

  def anio_en_curso_number
    return Date.today.year - anio_inicio + 1
  end

  def interes
    periodo_string = siguiente_periodo_a_pagar
    anio           = periodo_string.split('/')[0].to_i
    mes            = periodo_string.split('/')[1].to_i
    dia = 10
    fecha_limite = Date.new(anio, mes, dia)
    Date.today > fecha_limite ? 100 : 0
  end

  def registrar_pago(periodo_pago, monto, fecha)
    Payment.create(
      fecha: (fecha || Date.today), 
      monto_a_pagar: (study.monto_cuota - beca),
      course: self, 
      monto: monto, 
      periodo_pago: periodo_pago)

  end
end
