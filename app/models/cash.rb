# == Schema Information
#
# Table name: cashes
#
#  id            :integer          not null, primary key
#  fecha         :date
#  monto_inicial :decimal(, )
#  monto_final   :decimal(, )
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  cerrada       :boolean
#

class Cash < ActiveRecord::Base

  def ingresos
    {
      entries: Entry.where(fecha: self.fecha),
      payments: Payment.where(fecha: self.fecha)
    }
  end

  def total_ingresos
    all = ingresos
    sum = all[:entries].inject(0) { |sum, entry| sum + entry.monto  } 
    sum += all[:payments].inject(0) { |sum, payment| sum + payment.monto  }
    sum
  end

  def total_egresos
    egresos.inject(0) { |sum, expending| sum + expending.monto }
  end

  def egresos
    Expending.where(fecha: self.fecha)
  end

  def monto_final_real
    monto_final || (monto_inicial + total_ingresos - total_egresos)
  end

  def abierta?
    !cerrada
  end
end
