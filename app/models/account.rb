# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  nombre     :string
#  tipo       :string(15)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Account < ActiveRecord::Base

  GASTO   = 'GASTO'
  INGRESO = 'INGRESO'

  TIPOS = [GASTO, INGRESO]

  def self.de_gastos
    self.where(tipo: GASTO)
  end

  def self.de_gastos_no_de_carrera
    self.de_gastos.where.not(id: StudyExpendingAccount.pluck(:account_id))
  end

  def self.de_ingresos
    self.where(tipo: INGRESO)
  end

  def to_s
    nombre
  end
end
