# == Schema Information
#
# Table name: studies
#
#  id           :integer          not null, primary key
#  nombre       :string(100)
#  descripcion  :string
#  tipo_estudio :string(20)
#  duracion     :integer
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  monto_cuota  :decimal(, )      not null
#  requisitos   :text
#

class Study < ActiveRecord::Base

    
  has_many :study_expending_accounts
  has_many :cuentas_de_gastos, through: :study_expending_accounts, source: :account

  accepts_nested_attributes_for :cuentas_de_gastos

  CARRERA = "CARRERA"
  CURSO = "CURSO"

  TIPOS_ESTUDIO = [CARRERA, CURSO]

  validates_inclusion_of :tipo_estudio, :in => TIPOS_ESTUDIO, :on => :create, :message => "El tipo debe ser CARRERA  o CURSO"
  
  REQUISITOS_DISPONIBLES = %w{fotos DNI partida_de_nacimiento titulo_secundario certificado_buena_salud grupo_sanguineo}

  serialize :requisitos
  validates_presence_of :monto_cuota

  def self.carreras
    self.where(tipo_estudio: CARRERA).order(nombre: :asc)
  end

  def self.cursos
    self.where(tipo_estudio: CURSO).order(nombre: :asc)
  end

  def self.meses
    (1..12).map { |n|
      [n, I18n.l(Date.new(Date.today.year, n, 1), format: "%B").capitalize] 
    }
  end

  def es_requisito? req
    requisitos.include? req if requisitos
  end

  def duracion_to_s
    
  end

  def carrera?
    tipo_estudio == CARRERA
  end

  def curso?
    !carrera?    
  end

  def to_s
    nombre
  end

  def ingresos_anio(anio_inicio = nil, **params)
    params[:inicio] = (Date.today - 1000.years).to_s if params[:inicio].present? == false
    params[:fin]    = (Date.today + 1000.years).to_s if params[:fin].present? == false

    if anio_inicio
      Payment.includes(:course => :study)
             .where(courses: {anio_inicio: anio_inicio, study_id: self.id})
             .where(fecha: params[:inicio]..params[:fin])
             .sum('payments.monto')
    else 
      Payment.includes(:course => :study)
             .where(courses: {study_id: self.id})
             .where(fecha: params[:inicio]..params[:fin])
             .sum('payments.monto')
    end
  end

  def sumar_gastos(**params)
    params[:inicio] = (Date.today - 1000.years).to_s if params[:inicio].present? == false
    params[:fin]    = (Date.today + 1000.years).to_s if params[:fin].present? == false

    if params[:cuenta].present?
      Expending.where(account: params[:cuenta], fecha: params[:inicio]..params[:fin]).sum(:monto)
    else
      Expending.where(fecha: params[:inicio]..params[:fin], account: cuentas_de_gastos).sum(:monto)
    end
  end

  def balance(**params)
    ingresos_anio(params) - sumar_gastos(params)
  end

  def gastos_entre(inicio, fin)
    if inicio.present? == false 
      inicio = Date.today - 1000.years
      inicio = inicio.to_s
    end
    if fin.present? == false
      fin = Date.today + 1000.years
      fin = fin.to_s
    end
    Expending.where(account_id: cuentas_de_gastos.pluck(:id), fecha: inicio..fin)
  end
end

