# == Schema Information
#
# Table name: entries
#
#  id         :integer          not null, primary key
#  fecha      :date
#  monto      :decimal(, )
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Entry < ActiveRecord::Base

  belongs_to :account

  validates_presence_of :fecha, :monto, :account_id
  

  def self.search(params)
    start_date = params[:start_date] || 100.years.ago
    end_date   = params[:start_date] || Date.today + 1000.years

    entries = Entry.where(fecha: start_date..end_date)

    entries = entries.where(account_id: params[:account_ids]) if params[:account_ids]
    entries
  end
end
