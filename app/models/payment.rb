# == Schema Information
#
# Table name: payments
#
#  id            :integer          not null, primary key
#  fecha         :date             not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  course_id     :integer          not null
#  monto         :decimal(, )
#  periodo_pago  :string(10)
#  cae           :string
#  nro_factura   :integer
#  nro_recibo    :integer
#  monto_a_pagar :decimal(, )
#

class Payment < ActiveRecord::Base
  
  belongs_to :course

  def self.periodos_para_anio(anio)
    raise Exception.new('¡Dame el año!') if anio.present? == false
    (1..12).map { |e| "#{anio}/#{e.to_s.rjust(2,"0")}" }  
  end

  def self.periodos_actuales
    periodos_para_anio Date.today.year
  end

  def saldo?
    monto < monto_a_pagar
  end

  def saldo
    monto_a_pagar - monto
  end

  def periodo
    arr = periodo_pago.split('/')
    {
      anio: arr.first,
      mes: arr.last
    }
  end

  def descripcion
    "#{course.study} #{periodo_pago}"
  end
end
