# == Schema Information
#
# Table name: students
#
#  id              :integer          not null, primary key
#  nombre_completo :string
#  dni             :string(15)
#  matricula       :integer
#  direccion       :string
#  comentarios     :text
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Student < ActiveRecord::Base

  has_many :courses

  validates_presence_of :dni, :nombre_completo, :direccion


  def self.search(params)     
    if params[:mes_adeudado].present? || params[:carrers_ids].present?
      ids_con_deuda = Course.students_ids_con_deuda_en(params[:mes_adeudado], params[:carrers_ids])
      self.where(id: ids_con_deuda)
    else 
      self.all
    end
  end

  def to_s
    nombre_completo    
  end

end
