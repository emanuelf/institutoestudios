# == Schema Information
#
# Table name: study_expending_accounts
#
#  id         :integer          not null, primary key
#  study_id   :integer
#  account_id :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class StudyExpendingAccount < ActiveRecord::Base
  belongs_to :study
  belongs_to :account
end
