Bravo.pkey                     = Rails.root.join('app/assets/afip/private.key')
Bravo.cert                     = Rails.root.join('app/assets/afip/produccion.crt')
Bravo.cuit                     = '33714762759'
Bravo.sale_point               = '0003'

#Bravo.pkey                     = Rails.root.join('app/assets/afip/emanuel/private.key')
#Bravo.cert                     = Rails.root.join('app/assets/afip/emanuel/homologacion.crt')
#Bravo.cuit                     = '20331611930'
#Bravo.sale_point               = '0004'
Bravo.default_concepto         = 'Servicios'
Bravo.default_moneda           = :peso
Bravo.default_documento        = 'CUIT'
Bravo.own_iva_cond             = :exento
#Bravo.own_iva_cond             = :responsable_monotributo
Bravo.openssl_bin              = '/usr/bin/openssl'
Bravo::AuthData.environment     = :production
#Bravo::AuthData.environment     = :test
