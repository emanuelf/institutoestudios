
Rails.application.routes.draw do

  get '/courses/planilla_asistencia/' => 'courses#planilla_asistencia', as: :planilla_asistencia
  get '/courses/planilla_notas/' => 'courses#planilla_notas', as: :planilla_notas
  resources :courses
  root to: 'dashboard#index'

  get 'studens/planillas'

  get '/' => 'dashboard#index', as: :dashboard
  get 'estadosdecuenta' => 'dashboard#estados_de_cuenta', as: :estados_de_cuenta

  resources :payments do
    member do 
      get 'recibo' => 'payments#gen_comprobante'
    end
  end
  resources :entries
  resources :expendings
  resources :accounts
  resources :studies
  resources :students
  resources :cashes do 
    collection do 
      get 'today' => 'cashes#today', as: :today
    end
  end

  namespace :api do 
    resources :courses do 
      collection do 
        post 'registrarpago' => 'courses#registrar_pago'
      end
    end
    post 'cashes/setmi' => 'cashes#set_monto_inicial'
    post 'cashes/cerrar' => 'cashes#cerrar'
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
