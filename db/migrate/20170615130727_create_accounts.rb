class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :nombre
      t.string :tipo, limit: 15

      t.timestamps null: false
    end
  end
end
