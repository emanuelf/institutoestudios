class AddMontoCuotaToStudies < ActiveRecord::Migration
  def change
    add_column :studies, :monto_cuota, :decimal, null: false
  end
end
