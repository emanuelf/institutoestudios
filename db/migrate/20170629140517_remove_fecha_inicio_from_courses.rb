class RemoveFechaInicioFromCourses < ActiveRecord::Migration
  def change
    remove_column :courses, :fecha_inicio, :date
  end
end
