class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.date :fecha, null: false
      t.integer :mes
      t.belongs_to :student, index: true, foreign_key: true
      t.belongs_to :study, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
