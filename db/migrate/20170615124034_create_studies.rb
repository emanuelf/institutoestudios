class CreateStudies < ActiveRecord::Migration
  def change
    create_table :studies do |t|
      t.string :nombre, limit: 100
      t.string :descripcion
      t.string :tipo_estudio, limit: 20
      t.integer :duracion

      t.timestamps null: false
    end
  end
end
