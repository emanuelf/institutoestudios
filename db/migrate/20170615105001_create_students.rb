class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string :nombre_completo
      t.string :dni, limit: 15
      t.integer :matricula
      t.string :direccion
      t.text :comentarios

      t.timestamps null: false
    end
  end
end
