class CreateStudyExpendingAccounts < ActiveRecord::Migration
  def change
    create_table :study_expending_accounts do |t|
      t.belongs_to :study, index: true, foreign_key: true
      t.belongs_to :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
