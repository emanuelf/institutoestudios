class AddBajaAndFechaBajaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :baja, :boolean
    add_column :courses, :fecha_baja, :date
  end
end
