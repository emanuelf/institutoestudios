class RemoveMesToPaymentsAndAddPeriodoPagoToPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :mes
    add_column :payments, :periodo_pago, :string, limit: 10
  end
end
