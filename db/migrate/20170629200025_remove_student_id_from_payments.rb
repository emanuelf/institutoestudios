class RemoveStudentIdFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :student_id, :integer
  end
end
