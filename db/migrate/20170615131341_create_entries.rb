class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.date :fecha
      t.decimal :monto
      t.belongs_to :account, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
