class CreateCashes < ActiveRecord::Migration
  def change
    create_table :cashes do |t|
      t.date :fecha
      t.decimal :monto_inicial
      t.decimal :monto_final

      t.timestamps null: false
    end
  end
end
