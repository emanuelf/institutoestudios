class AddMontoAPagarToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :monto_a_pagar, :decimal
  end
end
