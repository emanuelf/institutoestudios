class RemoveStudyIdFromPayments < ActiveRecord::Migration
  def change
    remove_column :payments, :study_id, :integer
  end
end
