class AddMontoToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :monto, :decimal
  end
end
