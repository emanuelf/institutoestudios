class AddBecaToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :beca, :decimal, default: 0
  end
end
