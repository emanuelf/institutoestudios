class AddCaeAndNroFacturaAndNroReciboToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :cae, :string
    add_column :payments, :nro_factura, :integer
    add_column :payments, :nro_recibo, :integer
  end
end
