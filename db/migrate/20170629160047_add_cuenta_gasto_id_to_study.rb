class AddCuentaGastoIdToStudy < ActiveRecord::Migration
  def change
    add_column :studies, :cuenta_gasto_id, :integer, index: true, null: false
    add_foreign_key :studies, :accounts, column: :cuenta_gasto_id
  end
end
