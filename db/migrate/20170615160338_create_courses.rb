class CreateCourses < ActiveRecord::Migration
  def change
    create_table :courses do |t|
      t.belongs_to :study, index: true, foreign_key: true
      t.belongs_to :student, index: true, foreign_key: true
      t.date :fecha_inicio

      t.timestamps null: false
    end
  end
end
