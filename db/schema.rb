# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170816013032) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "nombre"
    t.string   "tipo",       limit: 15
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "cashes", force: :cascade do |t|
    t.date     "fecha"
    t.decimal  "monto_inicial"
    t.decimal  "monto_final"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.boolean  "cerrada"
  end

  create_table "courses", force: :cascade do |t|
    t.integer  "study_id"
    t.integer  "student_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "anio_inicio"
    t.decimal  "beca",        default: 0.0
    t.text     "requisitos"
    t.boolean  "baja"
    t.date     "fecha_baja"
  end

  add_index "courses", ["student_id"], name: "index_courses_on_student_id", using: :btree
  add_index "courses", ["study_id"], name: "index_courses_on_study_id", using: :btree

  create_table "entries", force: :cascade do |t|
    t.date     "fecha"
    t.decimal  "monto"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "entries", ["account_id"], name: "index_entries_on_account_id", using: :btree

  create_table "expendings", force: :cascade do |t|
    t.date     "fecha"
    t.decimal  "monto"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "expendings", ["account_id"], name: "index_expendings_on_account_id", using: :btree

  create_table "payments", force: :cascade do |t|
    t.date     "fecha",                    null: false
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "course_id",                null: false
    t.decimal  "monto"
    t.string   "periodo_pago",  limit: 10
    t.string   "cae"
    t.integer  "nro_factura"
    t.integer  "nro_recibo"
    t.decimal  "monto_a_pagar"
  end

  add_index "payments", ["course_id"], name: "index_payments_on_course_id", using: :btree

  create_table "students", force: :cascade do |t|
    t.string   "nombre_completo"
    t.string   "dni",             limit: 15
    t.integer  "matricula"
    t.string   "direccion"
    t.text     "comentarios"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "studies", force: :cascade do |t|
    t.string   "nombre",       limit: 100
    t.string   "descripcion"
    t.string   "tipo_estudio", limit: 20
    t.integer  "duracion"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.decimal  "monto_cuota",              null: false
    t.text     "requisitos"
  end

  create_table "study_expending_accounts", force: :cascade do |t|
    t.integer  "study_id"
    t.integer  "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "study_expending_accounts", ["account_id"], name: "index_study_expending_accounts_on_account_id", using: :btree
  add_index "study_expending_accounts", ["study_id"], name: "index_study_expending_accounts_on_study_id", using: :btree

  add_foreign_key "courses", "students"
  add_foreign_key "courses", "studies"
  add_foreign_key "entries", "accounts"
  add_foreign_key "expendings", "accounts"
  add_foreign_key "payments", "courses"
  add_foreign_key "study_expending_accounts", "accounts"
  add_foreign_key "study_expending_accounts", "studies"
end
