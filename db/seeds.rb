# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

cuenta_gasto_carrera = Account.create(nombre: 'Tecnica en cosmetología', tipo: Account::GASTO)

carrera = Study.create(
  nombre: 'Tecnica en cosmetología', 
  tipo_estudio: 'CARRERA', 
  monto_cuota: 1000)

carrera.cuentas_de_gastos << cuenta_gasto_carrera

carrera.requisitos = %w{fotos DNI partida_de_nacimiento titulo_secundario certificado_buena_salud grupo_sanguineo}

carrera.save


cuenta_gasto_curso = Account.create(nombre: 'Peluquería', tipo: Account::GASTO)

curso = Study.create(nombre: 'Peluquería', 
  tipo_estudio: 'CURSO', 
  monto_cuota: 700)


curso.cuentas_de_gastos << cuenta_gasto_curso

curso.requisitos = %w{fotos DNI}

curso.save

students_array = [
  {nombre_completo: 'Andruszyszyn, Stefani Thalia', dni: '41503704', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Antunez , Naara Yasmin', dni: '39226431', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Bakovski , Micaela Mariana', dni: '39638490', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Barboza , Adriana Soledad', dni: '33396633', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Batirola, Ana Paula', dni: '41302262', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Baumkratz, Camila Belen', dni: '38566782', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Breitembruch, Rosalia Noemi', dni: '37221710', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Bechmann, Patricia Susana', dni: '26487320', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Bloch, Mariana Abiyan', dni: '40199568', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Cerdan, Aldana Indira', dni: '40903494', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Da Silva , MarianA Anabel', dni: '37223860', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Dutra , Romina Mariela', dni: '40410832', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Faundez Krause, Ingrid Nahara', dni: '36458370', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Ferrero , Melany Jacqueline', dni: '40956344', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Gomez, Gabriela Alejandra', dni: '36466803', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Gomez , Sandra Marcela', dni: '36471270', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Gonzalez, Flavia Patricia', dni: '38777518', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Gonzalez , Noelia Alejandra', dni: '28157277', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Groff , Milagros Agustina', dni: '40150780', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Hillesheim, Antonella Elizabeth', dni: '39221290', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Maciel, Daiana Dalila', dni: '37893662', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Marquez , Angela Analinda', dni: '30560497', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Nazaroff, Noelia Ines', dni: '39638404', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Pereyra, Samantha Leonela', dni: '36458155', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Rios , Alba Irene', dni: '30255077', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Samaniego Olexen, Daiana Agustina', dni: '40952136', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Scheibner, Sonia', dni: '19042832', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Tabares , Yessica Beatriz', dni: '39879092', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Ullrich, Evelyn Yisel', dni: '39819038', anio: 'Primero', carrera: 'Superior'},
  {nombre_completo: 'Alvez , Carolain Jeniffer', dni: '41235773', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Behr , Maria Lucia', dni: '30871576', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Bourscheid , Paola Belen', dni: '39819700', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Chrzanowski , Paola Alexandra', dni: '41091951', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Diaz , Priscila Cesia', dni: '37951072', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Dos Santos , Alejandra Itati', dni: '41091638', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Fleitas , Oriana Magali', dni: '40195633', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Gonzalez Ortiz , Rosana Paola', dni: '25146261', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Gonzalez , Florencia', dni: '38775757', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Horchuk , Maira Soledad', dni: '41048875', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Kappen , Noelia Soledad', dni: '35370177', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Lopez , Cecilia Noemi', dni: '32844616', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Moreta , Maria Fernanda', dni: '37950683', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Ratoski , Angelica Graciela', dni: '39819668', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Ramirez , Gladys Luciana', dni: '34393940', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Rusch , Gladys Beatriz', dni: '24508000', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Velazco , Luciana Soledad', dni: '40798591', anio: 'Segundo', carrera: 'superior'},
  {nombre_completo: 'Cortinez Zabala , Johanna Elizabeth', dni: '35961842', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Chalanchuk , Liliana Raquel', dni: '23897209', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Da Luz , Maria Pamela', dni: '39220879', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Gimenez , Andrea Belen', dni: '36471526', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Günther , Cindy Maria Laura', dni: '29813635', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Izi , Ailin Paola', dni: '37160051', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Kozicki , Natalia Romina', dni: '32367598', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Martinez , Mirian Gisel', dni: '35453835', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Mendez , Juliana Celestina', dni: '36059302', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Nuñez , Celeste Alejandra', dni: '39819655', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Nuñez , Magdalena Elizabeth', dni: '38198493', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Figueroa , Aldana Soledad', dni: '38568252', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Otto , Camila del Carmen', dni: '30717305', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Resner , Lilian Mariel', dni: '39526914', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Sosa , Andrea Rocio', dni: '37083454', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Werner , Karen Marilyn', dni: '39723817', anio: 'Tercero', carrera: 'superior'},
  {nombre_completo: 'Aman , Adela Ester', dni: '39638568', anio: 'primero', carrera: ''},
  {nombre_completo: 'Benitez , Rosana Soledad', dni: '30903718', anio: 'primero', carrera: ''},
  {nombre_completo: 'De Lima , Rossana Patricia', dni: '35010842', anio: 'primero', carrera: ''},
  {nombre_completo: 'Dos Santos,  Rossely', dni: '30360067', anio: 'primero', carrera: ''},
  {nombre_completo: 'Gomez , Mariela Emiliana', dni: '34003529', anio: 'primero', carrera: ''},
  {nombre_completo: 'Günther , Patricia Valeria', dni: '36553643', anio: 'primero', carrera: ''},
  {nombre_completo: 'Lorenzo , Patricia Raquel', dni: '34356061', anio: 'primero', carrera: ''},
  {nombre_completo: 'Melho,  Ruben Oscar', dni: '38568750', anio: 'primero', carrera: ''},
  {nombre_completo: 'Ojeda , Santa Rita', dni: '33572117', anio: 'primero', carrera: ''},
  {nombre_completo: 'Retamoso , Natalia Isabel', dni: '39639169', anio: 'primero', carrera: ''},
  {nombre_completo: 'Sosa , Paola Soledad', dni: '33406496', anio: 'primero', carrera: ''},
  {nombre_completo: 'Tissera , Cynthia Muriel', dni: '36988403', anio: 'segundo', carrera: ''},
  {nombre_completo: 'Vega , Karina Gisel', dni: '36061879', anio: 'primero', carrera: ''},
  {nombre_completo: 'Viera,  Gabriela Andrea', dni: '32940799', anio: 'primero', carrera: ''},
  {nombre_completo: 'Asselborn , Nataly Jeanette', dni: '32189290', anio: 'primero', carrera: ''},
  {nombre_completo: 'Barbaro , Carolina Florencia', dni: '35776707', anio: 'primero', carrera: ''},
  {nombre_completo: 'Da Silva , Lourdes Adriana', dni: '34735288', anio: 'primero', carrera: ''},
  {nombre_completo: 'Escobar , Claudia Idalina', dni: '32040217', anio: 'primero', carrera: ''},
  {nombre_completo: 'Fernandez , Angela Vanina', dni: '31790021', anio: 'primero', carrera: ''},
  {nombre_completo: 'Fröm , Maria Ofelia', dni: '35695959', anio: 'primero', carrera: ''},
  {nombre_completo: 'Lucero , Paula Micaela', dni: '35015421', anio: 'primero', carrera: ''},
  {nombre_completo: 'Marquez, Emilia Soledad', dni: '30480585', anio: 'primero', carrera: ''},
  {nombre_completo: 'Ortiz , Marcela Alejandra', dni: '35015302', anio: 'primero', carrera: ''},
  {nombre_completo: 'Pimentel , Andrea Elizabeth', dni: '36461412', anio: 'primero', carrera: ''},
  {nombre_completo: 'Rivero , Laura Analia', dni: '40042037', anio: 'primero', carrera: ''},
  {nombre_completo: 'Beker, Paola Yesica', dni: '38064576', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Correa, Cristian Jonatan', dni: '35461973', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Dos Santos, Paola Yesica', dni: '41501925', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Kapelinski , Vanesa', dni: '37950274', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Maidana , Sandra Soledad', dni: '34002625', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Marchiotti, Daniela Lorena', dni: '32512658', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Marchiotti, Valeria Yania', dni: '35695439', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Medina, jenifer Daiana', dni: '36497515', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Morel, Nancy', dni: '25303943', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Nake, Elizabeth', dni: '29552221', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Paul, Silvia Gisela', dni: '38381436', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Pereira, Eliana Estefania', dni: '35011642', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Reineck, Patricia Soraya', dni: '27661426', anio: 'Primero', carrera: ''},
  {nombre_completo: 'Tomaro, Maria Fernanda', dni: '26166487', anio: 'Primero', carrera: ''}
]

students_array.each do |hash|
  anio = Date.today.year
  if hash[:anio].strip == 'Segundo'
    anio -=1
  elsif hash[:anio].strip == 'Tercero'
    anio -=2
  end
  student = Student.create(nombre_completo: hash[:nombre_completo], dni: hash[:dni], direccion: 'Falta cargar')
  curso_o_carrera = hash[:carrera].empty? ? curso : carrera
  Course.create(study: curso_o_carrera, student: student, anio_inicio: anio, requisitos: [])
end
